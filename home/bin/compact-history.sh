#!/bin/bash

set -e

cp ~/.bash_history ~/.bash_history.backup-$(date '+%Y-%m-%d-%H-%M-%S')
tail -r ~/.bash_history | awk '!x[$0]++' | tail -r > ~/.bash_history.rev_uniq
wc -l ~/.bash_history.rev_uniq ~/.bash_history
mv ~/.bash_history.rev_uniq ~/.bash_history
