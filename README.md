# edot -- Elliott Slaughter's personal dotfiles configuration

This is my personal dotfiles configuration tool for bash, git, hg,
screen, tmux, etc. The tool was not written with general purpose use
in mind, and as such the settings within reflect my personal
preferences. If you find it (or parts of it) useful, please feel free
to fork it and configure it to your own tastes (and, if you think your
changes might be of interest to a broader audience, submit a pull
request to contribute them back to the upstream repository).

## Installation

Invoke the installer to symlink the appropriate dotfiles into your
home directory. The installer is interactive and will prompt you for a
name and email, and will ask whether or not to override existing files
in your home directory on a case-by-case basis. Any files that you
choose to override will first be copied to a file with the suffix
`.edot_saved`.

    ./install.py

## System-Specific Customization

For system-specific configuration, edot looks for files with the
suffix `.local`. So for example, instead of modifying `.bashrc`
directly, you can modify `.bashrc.local`. This both preserves the
symlinks (so that updates to edot automatically apply to your dotfiles
without needing to rerun the installer), and means that you don't need
to commit system-specific configurations into git.

## License

The files under `external` are taken from external projects and have
their own licenses. All other files are provided under the license
below:

Copyright (c) 2011-2022, Elliott Slaughter <elliottslaughter@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
