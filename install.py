#!/usr/bin/env python3

from __future__ import print_function
import argparse, json, os, platform, shutil

# TODO: Add upgrade mechanism for checking interpolated file contents
# TODO: Not obvious that install and update need separate paths

_system_name = platform.system()

# Paths
_root_dir = os.path.abspath(os.path.dirname(__file__))
_src_dir = _root_dir
_config_file = os.path.join(_root_dir, 'conf.json')
_install_dir = os.path.expanduser('~')

# User-specific settings to query at install-time
_queries = [
    ('Name', 'Your Name'),
    ('Email', 'your@email.address'),
]
_config = {}

# Files
_files = [
    # home
    ('symlink', ('home/bash_profile', '.bash_profile')),
    ('symlink', ('home/bashrc', '.bashrc')),
    ('symlink', ('home/inputrc', '.inputrc')),
    ('symlink', ('home/gdbinit', '.gdbinit')),
    ('interpolate', ('home/gitconfig', '.gitconfig')),
    ('symlink', ('external/git/contrib/completion/git-completion.bash',
                 '.git-completion.bash')),
    ('interpolate', ('home/hgrc', '.hgrc')),
    ('symlink', ('home/screenrc', '.screenrc')),
    ('symlink', ('home/tmux.conf', '.tmux.conf')),

    # home/bin
    ('mkdir', ('bin',)),
    ('symlink', ('home/bin/compact-history.sh', 'bin/compact-history.sh')),
]

# Handlers
def prompt_overwrite(interact, force, dst_path):
    if force: return True
    if not interact:
        raise Exception('Running in non-interactive mode but need user input')
    overwrite = input('"%s" already exists, overwrite (y/[n])? ' % dst_path)
    return overwrite == 'yes' or overwrite == 'y'

if _system_name == 'Windows':
    def symlink(config, interact, force, src_dir, dst_dir, src_file, dst_file):
        src_path = os.path.join(src_dir, src_file)
        dst_path = os.path.join(dst_dir, dst_file)
        if os.path.exists(dst_path):
            if not prompt_overwrite(interact, force, dst_path): return False
            save_path = '%s.edot_saved' % dst_path
            os.rename(dst_path, save_path)
            print('Saved old "%s" as "%s"' % (dst_path, save_path))
        shutil.copy2(src_path, dst_path)
        return True
else:
    def symlink(config, interact, force, src_dir, dst_dir, src_file, dst_file):
        src_path = os.path.join(src_dir, src_file)
        dst_path = os.path.join(dst_dir, dst_file)
        if os.path.lexists(dst_path):
            if os.path.islink(dst_path) and \
               os.path.realpath(dst_path) == src_path:
                return False
            if not prompt_overwrite(interact, force, dst_path): return False
            save_path = '%s.edot_saved' % dst_path
            os.rename(dst_path, save_path)
            print('Saved old "%s" as "%s"' % (dst_path, save_path))
        os.symlink(src_path, dst_path)
        return True

def interpolate(config, interact, force, src_dir, dst_dir, src_file, dst_file):
    src_path = os.path.join(src_dir, src_file)
    dst_path = os.path.join(dst_dir, dst_file)
    if os.path.exists(dst_path):
        if not prompt_overwrite(interact, force, dst_path): return False
        save_path = '%s.edot_saved' % dst_path
        os.rename(dst_path, save_path)
        print('Saved old "%s" as "%s"' % (dst_path, save_path))
    with open(src_path, 'r') as fi:
        with open(dst_path, 'w') as fo:
            t = fi.read()
            for k, v in config.items():
                t = t.replace('@@%s@@' % k, v)
            fo.write(t)
    return True

def mkdir(config, interact, force, src_dir, dst_dir, dst_subdir):
    dst_path = os.path.join(dst_dir, dst_subdir)
    if os.path.isdir(dst_path):
        return False
    os.mkdir(dst_path)
    return True

def nop(*args):
    return False

_install_handlers = {
    'symlink': symlink,
    'interpolate': interpolate,
    'mkdir': mkdir,
    }

_upgrade_handlers = {
    'symlink': symlink,
    'interpolate': interpolate,
    'mkdir': mkdir,
    }

def load_config(filename):
    with open(filename, 'r') as f: return json.load(f)

def write_config(config, filename):
    with open(filename, 'w') as f: json.dump(config, f)

def query_config(interact, initial_values, queries):
    values = []
    for query, default in queries:
        if query in initial_values:
            values.append((query, initial_values[query]))
        else:
            if not interact:
                raise Exception(
                    'Running in non-interactive mode but need user input')
            values.append((query,
                           input('%s (%s)? ' % (query, default)) or
                           default))
    return dict(values)

def is_installed():
    return os.path.exists(_config_file)

def install(interact, force):
    changed = any([_install_handlers[handler](_config, interact, force,
                                              _src_dir, _install_dir, *args)
                   for (handler, args) in _files])
    if not changed:
        print('Nothing to do right now.')

def upgrade(interact, force):
    changed = any([_upgrade_handlers[handler](_config, interact, force,
                                              _src_dir, _install_dir, *args)
                   for (handler, args) in _files])
    if not changed:
        print('Nothing to do right now.')

def main():
    global _config

    parser = argparse.ArgumentParser(
        description = 'edot installer')

    parser.add_argument(
        '--non-interactive', dest='interact', action='store_false',
        help='Never prompt the user for input.')

    parser.add_argument(
        '--query', dest='query_args', action='append', default=[],
        help='Supply value for query.')

    parser.add_argument(
        '--force', dest='force', action='store_true',
        help='Force override of files.')

    args = parser.parse_args()

    queries = dict(_queries)
    initial_values = []
    for arg in args.query_args:
        try:
            index = arg.index('=')
        except ValueError:
            raise Exception('Malformed query value %s' % arg)
        query, value = arg[:index], arg[index+1:]
        if not query in queries:
            raise Exception('Unrecognized query %s' % query)
        initial_values.append((query, value))
    initial_values = dict(initial_values)

    if is_installed():
        print('Hi again!')
        print()
        _config = load_config(_config_file)
        upgrade(args.interact, args.force)
    else:
        print('Hi, my name is edot. Who are you?')
        print()
        _config = query_config(args.interact, initial_values, _queries)
        print()
        write_config(_config, _config_file)
        install(args.interact, args.force)

if __name__ == '__main__':
    main()
